package com.example.order.clients;

import com.example.order.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
@FeignClient(name = "user-service", url = "http://localhost:8080")
public interface UserFeignClient {
    @GetMapping("/user/{id}")
    User findById(@PathVariable String id);

    @GetMapping("/user/getUserFromToken")
    User getUser();

    @GetMapping("/user/getAll")
    List<User> getAllUsers();
}
