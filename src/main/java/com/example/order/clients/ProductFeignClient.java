package com.example.order.clients;

import com.example.order.entities.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(name = "product-service", url = "http://localhost:8080/product")
public interface ProductFeignClient {
    @GetMapping("/getAll")
    List<Product> findAllProducts();

    @GetMapping("/updateStock")
    Product updateStock(@RequestParam String id, @RequestParam int quantity);
}
