package com.example.order.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class FullOrderResponse {
    private String orderId;
    private UserOrderResponse customer;
    private List<FullOrderItem> items;
    private double totalPrice;
    private LocalDateTime orderDate;
}
