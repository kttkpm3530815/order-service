package com.example.order.dtos;

import com.example.order.entities.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullOrderItem {
    private String productId;
    private String name;
    private double price;
    private int quantity;
}
