package com.example.order;

import com.example.order.clients.UserFeignClient;
import com.example.order.dtos.FullOrderResponse;
import com.example.order.dtos.OrderRequest;
import com.example.order.entities.Order;
import com.example.order.services.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/order")
public class OrderController {
    private final OrderService orderService;
    private final UserFeignClient userFeignClient;

    @PostMapping("/order")
    @Caching(evict = {
            // Evict the all cache key "product" and "products"
            @CacheEvict(value = "products", allEntries = true),
            @CacheEvict(value = "product", key = "#request.items")

    })
    public Order placeOrder(@RequestBody OrderRequest request) {
//        System.out.println(request);
        log.info("path=/order/order");
        log.info("Order request: {}", request);
        return orderService.placeOrder(request);
    }

    @GetMapping("/findAll")
    public List<Order> findAllOrders() {
        log.info("path=/order/findAll");
        log.info("Finding all orders");
        return orderService.findAllOrders();
    }

    @GetMapping("/findAllFullOrders")
    public List<FullOrderResponse> findAllFullOrders() {
        log.info("path=/order/findAllFullOrders");
        log.info("Finding all full orders");
        return orderService.findAllFullOrders();
    }

    @GetMapping("/findFullOrder/{orderId}")
    public FullOrderResponse findFullOrderById(@PathVariable String orderId) {
        log.info("path=/order/findFullOrder/{orderId}");
        log.info("Finding full order by id: orderId={}", orderId);
        User user = userFeignClient.getUser();
        return orderService.findFullOrderById(orderId, user);
    }

    @GetMapping("/findFullOrderByUser/{userId}")
    public List<FullOrderResponse> findFullOrderByUser(@PathVariable String userId) {
        log.info("path=/order/findFullOrderByUser/{userId}");
        log.info("Finding full order by user: userId={}", userId);
        return orderService.findFullOrderByUser(userId);
    }

    @GetMapping("/find/{orderId}")
    public Order findOrderById(String orderId) {
        log.info("path=/order/find/{orderId}");
        log.info("Finding order by id: orderId={}", orderId);
        return orderService.findOrderById(orderId);
    }

}
