package com.example.order.services;

import com.example.order.OrderRepository;
import com.example.order.User;
import com.example.order.clients.ProductFeignClient;
import com.example.order.clients.UserFeignClient;
import com.example.order.dtos.FullOrderItem;
import com.example.order.dtos.FullOrderResponse;
import com.example.order.dtos.OrderRequest;
import com.example.order.dtos.UserOrderResponse;
import com.example.order.entities.Order;
import com.example.order.entities.OrderItem;
import com.example.order.entities.Product;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor

public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductFeignClient productFeignClient;
    private final CacheService myService;
    private final UserFeignClient userFeignClient;

//    @Retry(name = "orderService")
    public Order placeOrder(OrderRequest request) {

        Order order = new Order();

        List<Product> productList = productFeignClient.findAllProducts();

        List<OrderItem> orderItems = request.getItems().stream().map(item -> {
            Product product = productList.stream().filter(p -> p.getId().equals(item.getProductId())).findFirst().orElse(null);
            if (product != null) {
                OrderItem orderItem = new OrderItem();
                orderItem.setProductId(product.getId());
                orderItem.setQuantity(item.getQuantity());
                orderItem.setPrice(product.getPrice());
                return orderItem;
            }
            return null;
        }).toList();
        order.setOrderDate(LocalDateTime.now());
        order.setCustomerId(request.getCustomerId());
        order.setItems(orderItems);
        order.setTotalPrice(orderItems.stream().mapToDouble(item -> item.getPrice() * item.getQuantity()).sum());

        orderItems.forEach(item -> {
            productFeignClient.updateStock(item.getProductId(), -item.getQuantity());
        });

        myService.evictMultipleCacheValues(orderItems.stream().map(OrderItem::getProductId).toList());

        return orderRepository.save(order);
    }

    public Order findOrderById(String orderId) {
        return orderRepository.findById(orderId).orElse(null);
    }

    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    @CircuitBreaker(name = "orderService")
    public List<FullOrderResponse> findAllFullOrders() {
        List<Order> orders = orderRepository.findAll();
        List<Product> productList = productFeignClient.findAllProducts();
        List<User> users = userFeignClient.getAllUsers();
        return orders.stream().map(order -> {
//            List<Product> productList = productFeignClient.findAllProducts();
            List<FullOrderItem> fullOrderItems = order.getItems().stream().map(item -> {
                Product product = productList.stream().filter(p -> p.getId().equals(item.getProductId())).findFirst().orElse(null);
                if (product != null) {
                    return new FullOrderItem(product.getId(),
                            product.getName(), item.getPrice(),item.getQuantity());
                }
                return null;
            }).toList();
            User user = users.stream().filter(u -> u.getId().equals(order.getCustomerId())).findFirst().orElse(null);
            return new FullOrderResponse(
                    order.getId(),
                    new UserOrderResponse(
                            user.getId(),
                            user.getEmail(),
                            user.getFullName()

                    ),
                    fullOrderItems,
                    order.getTotalPrice(),
                    order.getOrderDate());
        }).toList();
    }

    @RateLimiter(name = "orderService")
    public FullOrderResponse findFullOrderById(String orderId, User user) {
        Order order = orderRepository.findById(orderId).orElse(null);
        if (order == null) {
            return null;
        }
        if(!user.getRole().equals("ADMIN") || !order.getCustomerId().equals(user.getId())) {
            return null;
        }
        List<Product> productList = productFeignClient.findAllProducts();
        List<FullOrderItem> fullOrderItems = order.getItems().stream().map(item -> {
            Product product = productList.stream().filter(p -> p.getId().equals(item.getProductId())).findFirst().orElse(null);
            if (product != null) {
                return new FullOrderItem(product.getId(),
                        product.getName(), item.getPrice(),item.getQuantity());
            }
            return null;
        }).toList();
        return new FullOrderResponse(
                order.getId(),
                new UserOrderResponse(
                        user.getId(),
                        user.getFullName(),
                        user.getEmail()
                ),
                fullOrderItems,
                order.getTotalPrice(),
                order.getOrderDate());
    }

    @Retry(name = "orderService")
    public List<FullOrderResponse> findFullOrderByUser(String userId) {
        User user = userFeignClient.findById(userId);
        List<Order> orders = orderRepository.findAllByCustomerId(userId);
        List<Product> productList = productFeignClient.findAllProducts();
        return orders.stream().map(order -> {
            List<FullOrderItem> fullOrderItems = order.getItems().stream().map(item -> {
                Product product = productList.stream().filter(p -> p.getId().equals(item.getProductId())).findFirst().orElse(null);
                if (product != null) {
                    return new FullOrderItem(product.getId(),
                            product.getName(), item.getPrice(),item.getQuantity());
                }
                return null;
            }).toList();
//            User user = userFeignClient.findById(order.getCustomerId());
            return new FullOrderResponse(
                    order.getId(),
                    new UserOrderResponse(
                            user.getId(),
                            user.getEmail(),
                            user.getFullName()
                    ),
                    fullOrderItems,
                    order.getTotalPrice(),
                    order.getOrderDate());
        }).toList();
    }
}
