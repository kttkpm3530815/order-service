package com.example.order.services;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CacheService {
    @CacheEvict(value = "product", key = "#key", allEntries = false)
    public void evictSingleCacheValue(String key) {
        // This method will be used to evict a single key
    }

    public void evictMultipleCacheValues(List<String> keys) {
        for (String key : keys) {
            evictSingleCacheValue(key);
        }
    }
}
